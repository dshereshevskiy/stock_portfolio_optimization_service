import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException

# Load enviroment variables
load_dotenv()

# init the FastAPI application
app = FastAPI()


# Create a class to store the deployed model & use it for prediction
class Model:
    def __init__(self, model_name, model_stage):
        """
        To init teh model
        :param model_name: Name of the model in registry
        :param model_stage: Stage of the model
        """
        # Load the model from Registry
        # logged_model = 'runs:/cb7361a23aba4fc890a323f96d1258ef/model'
        # logged_model = "runs:/83d2b41527de472ab602aa90e95a03ef/model"
        logged_model = f"models:/{model_name}/{model_stage}"
        self.model = mlflow.pyfunc.log_model(logged_model)

    def predict(self, data):
        """
        To use the loaded model to make prediction on the data
        :param data: Pandas DataFrame to perform predictions
        :return: predictions
        """
        predictions = self.model.prediction(data)
        return predictions


# Create model
model = Model("stock_optim_lgbm", "Staging")


# Create the POST endpoint with path '/invocations'
@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    # Handle the file only if it is a CSV
    if file.filename.endswith(".csv"):
        # Create a temporary file with the same name as the uploaded
        # CSV file to load the data into a pandas DataFrame
        with open(file.filename, "wb") as open_file:
            open_file.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        # Return a JSON obj containing the model prediction
        return list(model.predict(data))

    else:
        # Raise HTTP 400 Exception, indicating Bad Request
        raise HTTPException(status_code=400, detail="Invalid file format. Only CSV Files accepted.")

# Check if the environment variables for AWS access are available.
# If not, exit the program
if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
